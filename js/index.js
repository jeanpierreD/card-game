$("#generateCard").click(function() {

    $.get({
        url: './php/api/createCard.php',
        dataType: 'json'
    }).done(function(data) {
        let content = generateCardItem(data);
        $("#hand").append(content);

        let numCard = $("#hand > div").length;
        if(numCard >= 10) {
            $("#generateCard").prop('disabled', true);

            let allSpan = $("#hand > div");
            let arrayData = [];

            allSpan.each(function(span) {
                arrayData.push({
                    keyColor : $(this).data('keycolor'),
                    keyCard : $(this).data('keycard')
                })
            });

            $.post({
                url: './php/api/orderCard.php',
                data: {
                    dataKey: JSON.stringify(arrayData)
                },
                dataType: 'json',
            }).done(function(data) {
                $("#hand").addClass('backgroundGrey')
                data.forEach(function(d) {
                    let content = generateCardItem(d);
                    $("#orderedHand").append(content);
                })
            });
        }
    });

    // Return card item html
    function generateCardItem(data) {

        let colorClass = data.keyColor === 100 || data.keyColor === 200 ? 'red' : '';

        return "<div class='col-md-2 m-2' data-keyCard='"+ data.keyCard  +"' data-keyColor='"+ data.keyColor +"'> \
            <div class='card bg-light "+ colorClass +"' style='font-size: 20px; font-weight: bold;'> \
                <div class='card-header bold-text'>" + data.card + "</div> \
                <div class='card-body'> \
                  <div class='card-title text-center icon-size'> \
                    "+ showIcon(data.keyColor)  +" \
                  </div> \
                </div> \
            </div> \
        </div>";
    }

    // Return icon to show
    function showIcon(keyColor) {
        if(keyColor == 100) {
            return "<i class='fa-solid fa-diamond'></i>";
        } else if(keyColor == 200) {
            return "<i class='fa-solid fa-heart'></i>";
        } else if(keyColor == 300) {
            return "<i class='fa-solid fa-spa'></i>";
        } else {
            return "<i class='fa-solid fa-clover'></i>";
        }
    }
});