<?php

// Create board
class Board {

    // Get all colors
    function getColors() {
        return [
            100 => "Carreaux",
            200 => "Coeur",
            300 => "Pique",
            400 => "Trèfle"
        ];
    }

    // Get all cards
    function getCards() {
        return [
            2 => 2,
            3 => 3,
            4 => 4,
            5 => 5,
            6 => 6,
            7 => 7,
            8 => 8,
            9 => 9,
            10 => 10,
            11 => "Valet",
            12 => "Dame",
            13 => "Roi",
            14 => "As",
        ];
    }

    // Get card by keyColor & keyCard
    function getCard($keyCard) {
        return $this->getCards()[$keyCard];
    }

    // Get ordered hand
    function getOrderedHand($arrayHand) {
        // Create new array with compute key to order
        $data = array_map(function($hand) {
            return [
                'key' => $hand['keyColor'] + $hand['keyCard'],
                'keyCard' => $hand['keyCard'],
                'keyColor' => $hand['keyColor'],
                'card' => $this->getCard($hand['keyCard'])
            ];
        }, $arrayHand);

        // Get column key
        $keys = array_column($data, 'key');

        // Ordering multidimensional array
        array_multisort($keys, SORT_ASC, $data);

        // return ordering multidimensional array
        return array_map(function($card) {
            return [
                'keyCard' => $card['keyCard'],
                'keyColor' => $card['keyColor'],
                'card' => $this->getCard($card['keyCard'])
            ];
        }, $data);
    }

    // Get a randomn card
    function getRandomnCard() {
        // Generate randomn key color & card
        $keyColor = array_rand($this->getColors());
        $keyCard = array_rand($this->getCards());

        return [
            'keyCard' => $keyCard,
            'keyColor' => $keyColor,
            'card' => $this->getCard($keyCard)
        ];
    }
}

$board = new Board();