<?php

if($_SERVER['REQUEST_METHOD'] === 'POST') {
    if(isset($_POST['dataKey'])) {
        include ('../Board.php');

        $arrayHand = json_decode($_POST['dataKey'], true);
    
        echo json_encode($board->getOrderedHand($arrayHand));
    }
}

?>